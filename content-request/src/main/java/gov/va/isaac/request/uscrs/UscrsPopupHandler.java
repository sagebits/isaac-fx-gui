package gov.va.isaac.request.uscrs;

import java.io.File;
import java.io.IOException;
import java.util.stream.IntStream;
import javax.inject.Named;
import org.glassfish.hk2.api.PerLookup;
import org.jvnet.hk2.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import gov.va.isaac.AppContext;
//import gov.va.isaac.gui.listview.operations.OperationResult;
import gov.va.isaac.interfaces.gui.constants.SharedServiceNames;
import gov.va.isaac.interfaces.gui.views.commonFunctionality.ExportTaskHandlerI;
import gov.va.isaac.interfaces.gui.views.commonFunctionality.ExportTaskViewI;
import gov.va.isaac.interfaces.utility.DialogResponse;
import gov.va.isaac.util.Utility;
import gov.vha.isaac.ochre.api.Get;
import gov.vha.isaac.ochre.api.LookupService;
import javafx.concurrent.Task;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Window;

@Service
@Named(value = SharedServiceNames.USCRS)
@PerLookup
public class UscrsPopupHandler implements ExportTaskViewI {
	
	IntStream conceptStream;
	
	private static final Logger LOG = LoggerFactory.getLogger(UscrsContentRequestHandler.class);
	
	@Override
	public IntStream getConcepts() {
		return conceptStream;
	}

	@Override
	public void setConcepts(IntStream conceptInput) {
		conceptStream = conceptInput;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * gov.va.isaac.interfaces.gui.views.PopupViewI#showView(javafx.stage.Window)
	 */
	@Override
	public void showView(Window parent)
	{
		String firstDesc = Get.conceptDescriptionText(getConcepts().findFirst().getAsInt());
		
		DialogResponse verifyConceptSelelction = AppContext.getCommonDialogs().showYesNoDialog("USCRS Content Request", "You are exporting " + firstDesc + ". Would you like to continue?");
		
		if(verifyConceptSelelction == DialogResponse.NO) {
			return;
		}
		
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Save USCRS Concept Request File");
		fileChooser.getExtensionFilters().addAll(new ExtensionFilter("Excel Files .xls .xlsx", "*.xls", "*.xlsx"));
		fileChooser.setInitialFileName("USCRS_Export.xls");
		File file = fileChooser.showSaveDialog(null);

		try
		{
			//TODO fix this
			/* if ((firstConcept.getPathNid() != TermAux.SNOMED_CORE.getLenient().getNid())
					&& !OTFUtility.getConceptVersion(firstConcept.getPathNid()).getPrimordialUuid().toString().equals(AppContext.getAppConfiguration().getDefaultEditPathUuid())) */
			boolean isOnPath = true;
			if(isOnPath)
			{
				DialogResponse response = AppContext.getCommonDialogs().showYesNoDialog(
						"USCRS Content Request",
						"Concept Nid: "  + "The concept path is neither Snomed CORE nor DEVELOPMENT" 
								+ ". It is recommended that you only submit " + "concepts edited on one of these paths to USCRS.\n\n" + "Do you want to continue?");
				if (response == DialogResponse.NO)
				{
					return;
				}
			}
		}
		catch (Exception e)
		{
			AppContext.getCommonDialogs().showErrorDialog("USCRS Content Request", "Unable to load concepts for path comparison.", "This should never happen");
			return;
		}

		try
		{
			ExportTaskHandlerI uscrsExporter = LookupService.getService(ExportTaskHandlerI.class, SharedServiceNames.USCRS);
			if(uscrsExporter != null) {
				Task<Integer> task = uscrsExporter.createTask(conceptStream, file.toPath());
				Utility.execute(task);
				int count = task.get();
				AppContext.getCommonDialogs().showInformationDialog("USCRS Content Request", "Content request submission successful. Output: " + count + "\n\n Upload ");
			} else {
				throw new RuntimeException("The USCRS Content Request Handler is not available on the class path");
			}
		}
		catch (Exception e)
		{
			LOG.error("Unexpected error during submit", e);
			AppContext.getCommonDialogs().showErrorDialog("USCRS Content Request", "Unexpected error trying to submit request.", e.getMessage());
			return;
		}
	}

}
