/**
 * 
 */
package gov.va.isaac.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import gov.va.isaac.AppContext;
import gov.va.isaac.ExtendedAppContext;
import gov.va.isaac.config.profiles.UserProfileBindings;
import gov.vha.isaac.MetaData;
import gov.vha.isaac.ochre.api.ConceptProxy;
import gov.vha.isaac.ochre.api.Get;
import gov.vha.isaac.ochre.api.LookupService;
import gov.vha.isaac.ochre.api.chronicle.LatestVersion;
import gov.vha.isaac.ochre.api.component.concept.ConceptChronology;
import gov.vha.isaac.ochre.api.component.concept.ConceptSnapshot;
import gov.vha.isaac.ochre.api.component.concept.ConceptVersion;
import gov.vha.isaac.ochre.api.component.sememe.SememeChronology;
import gov.vha.isaac.ochre.api.component.sememe.SememeSnapshotService;
import gov.vha.isaac.ochre.api.component.sememe.version.DescriptionSememe;
import gov.vha.isaac.ochre.api.component.sememe.version.SememeVersion;
import gov.vha.isaac.ochre.api.constants.DynamicSememeConstants;
import gov.vha.isaac.ochre.api.coordinate.LanguageCoordinate;
import gov.vha.isaac.ochre.api.coordinate.PremiseType;
import gov.vha.isaac.ochre.api.coordinate.StampCoordinate;
import gov.vha.isaac.ochre.api.index.SearchResult;
import gov.vha.isaac.ochre.api.index.SememeIndexerBI;
import gov.vha.isaac.ochre.api.relationship.RelationshipVersionAdaptor;
import gov.vha.isaac.ochre.api.util.TaskCompleteCallback;
import gov.vha.isaac.ochre.impl.utility.Frills;
import gov.vha.isaac.ochre.impl.utility.SimpleDisplayConcept;
import gov.vha.isaac.ochre.model.configuration.StampCoordinates;
import gov.vha.isaac.ochre.query.provider.lucene.indexers.SememeIndexer;

/**
 * @author joel
 *
 */
public final class OchreUtility {
	private static final Logger LOG = LoggerFactory.getLogger(OchreUtility.class);

	private OchreUtility() {}

	/**
	 * Takes a string and returns the Semantic Tag (the string in parentheses)
	 * @param The string that contains a semantic tag in parentheses
	 * @return the semantic tag
	 * @throws Exception
	 */
	public static String getSemanticTag(String input) {
		if (input.indexOf('(') != -1) {
			String st = input.substring(input.lastIndexOf('(') + 1, input.lastIndexOf(')'));
			return st;
		} else {
			return "";
		}
	}
	public static String stripSemanticTag(String input) {
		if (input.lastIndexOf('(') != -1) {
			String st = input.substring(0, input.lastIndexOf('(')).trim();
			return st;
		} else {
			return "";
		}
	}
	/**
	 * Simple utility method to get the latest version of a concept without having to do the class conversion stuff
	 * @param conceptChronology the chronlogy to get the concept version for
	 * @param stampCoordinate - optional - if not provided, uses the current stamp coordinate from the user profile.
	 */
	@SuppressWarnings("unchecked")
	public static Optional<LatestVersion<? extends ConceptVersion<?>>> getLatestConceptVersion(ConceptChronology<? extends ConceptVersion<?>> conceptChronology, 
			StampCoordinate stampCoordinate) {
		@SuppressWarnings("rawtypes")
		ConceptChronology raw = (ConceptChronology)conceptChronology;
		
		return raw.getLatestVersion(ConceptVersion.class, 
				(stampCoordinate == null ? ExtendedAppContext.getUserProfileBindings().getStampCoordinate().get() : stampCoordinate));
	}

	/**
	 * Simple utility method to return a relationship version adapater, handling the class conversion stuff
	 * @param sememeChronology the relationship sememe chronology to lookup 
	 * @param stampCoordinate - optional - if not provided, uses the current stamp coordinate from the user profile
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Optional<LatestVersion<? extends RelationshipVersionAdaptor<?>>> getLatestRelationshipVersionAdaptor(
			SememeChronology<? extends RelationshipVersionAdaptor<?>> sememeChronology, StampCoordinate stampCoordinate) {
		@SuppressWarnings("rawtypes")
		SememeChronology rawSememChronology = (SememeChronology)sememeChronology;
		
		return rawSememChronology.getLatestVersion(RelationshipVersionAdaptor.class, 
				(stampCoordinate == null ? ExtendedAppContext.getUserProfileBindings().getStampCoordinate().get() : stampCoordinate));
	}

	/**
	 * @param id concept nid or sequence
	 * @param stampCoordinate - optional - if not provided, taken from user preferences.  StampCoordinate for retrieving the LogicGraphSememeImpl
	 * @param historical - if true, return all rels, if false, only return the latest
	 * @param premiseType PremiseTypes by which to filter. Passing null disables filtering by PremiseType (returns both STATED and INFERRED)
	 * @param relTypeSequence - optional - if provided - only rels that match the specified type sequence will be returned
	 * @return List of RelationshipVersionAdaptor
	 */
	public static List<RelationshipVersionAdaptor<?>> getRelationshipListOriginatingFromConcept(int id, 
			StampCoordinate stampCoordinate, boolean historical, PremiseType premiseType, Integer relTypeSequence) {
		List<RelationshipVersionAdaptor<?>> allRelationships = new ArrayList<>();
	
		List<? extends SememeChronology<? extends RelationshipVersionAdaptor<?>>> outgoingRelChronicles = Get.conceptService().getConcept(id)
				.getRelationshipListOriginatingFromConcept();
		for (SememeChronology<? extends RelationshipVersionAdaptor<?>> chronicle : outgoingRelChronicles)
		{
			if (historical) {
				for (RelationshipVersionAdaptor<?> rv : chronicle.getVersionList())
				{
					// Ensure that RelationshipVersionAdaptor corresponds to latest LogicGraph
					if ((premiseType == null || premiseType == rv.getPremiseType())
							&& (relTypeSequence == null || relTypeSequence == rv.getTypeSequence())) {
						allRelationships.add(rv);
						LOG.debug("getLatestRelationshipListOriginatingFromConcept(" + Get.conceptDescriptionText(id) + ", stampCoord, (PremiseType)" 
						+ (premiseType != null ? premiseType.name() : null) + ", relTypeSequence=" + relTypeSequence + ") adding " + rv);
					}
				}
			} else {
				Optional<LatestVersion<? extends RelationshipVersionAdaptor<?>>> latest = getLatestRelationshipVersionAdaptor(chronicle, 
						(stampCoordinate == null ? ExtendedAppContext.getUserProfileBindings().getStampCoordinate().get() : stampCoordinate));
				if (latest.isPresent() && latest.get().value() != null
						&& (premiseType == null || premiseType == latest.get().value().getPremiseType())
						&& (relTypeSequence == null || relTypeSequence == latest.get().value().getTypeSequence())) {
					allRelationships.add(latest.get().value());
					LOG.debug("getLatestRelationshipListOriginatingFromConcept(" + Get.conceptDescriptionText(id) + ", stampCoord, (PremiseType)" 
					+ (premiseType != null ? premiseType.name() : null) + ", relTypeSequence=" + relTypeSequence + ") adding " + latest.get().value());
				}
			}
		}
		return allRelationships;
	}
	
	/**
	 * Utility method to find all concepts present and active as a member of the PATHS_ASSEMBLAGE
	 * @return The path concepts
	 */
	public static Set<ConceptVersion<?>> getPathConcepts() {
		Stream<SememeChronology<? extends SememeVersion<?>>> sememes = Get.sememeService()
				.getSememesFromAssemblage(MetaData.PATHS_ASSEMBLAGE.getConceptSequence());

		final Set<ConceptVersion<?>> pathConcepts = new HashSet<>();
		Consumer<? super SememeChronology<? extends SememeVersion<?>>> action = new Consumer<SememeChronology<? extends SememeVersion<?>>>() {
			@Override
			public void accept(SememeChronology<? extends SememeVersion<?>> t) {
				ConceptChronology<? extends ConceptVersion<?>> pathCC = Get.conceptService().getConcept(t.getReferencedComponentNid());
				
				Optional<LatestVersion<? extends ConceptVersion<?>>> latestPathConceptVersion = getLatestConceptVersion(pathCC, StampCoordinates.getDevelopmentLatest());
				if (latestPathConceptVersion.isPresent()) {
					pathConcepts.add(latestPathConceptVersion.get().value());
				}
			}
		};
		sememes.distinct().forEach(action);

		if (pathConcepts.isEmpty()) {
			LOG.error("No paths loaded based on membership in {}", MetaData.PATHS_ASSEMBLAGE);
		} else {
			LOG.debug("Loaded {} paths: {}", pathConcepts.size(), pathConcepts);
		}
			
		return Collections.unmodifiableSet(pathConcepts);
	}
	

	/**
	 * @param nid concept nid (must be a nid)
	 * @param stamp - optional
	 * @return the text of the description, if found
	 */
	@SuppressWarnings("rawtypes")
	public static Optional<String> getFSNForConceptNid(int nid, StampCoordinate stamp)
	{
		SememeSnapshotService<DescriptionSememe> ss = Get.sememeService().getSnapshot(DescriptionSememe.class, 
				stamp == null ? ExtendedAppContext.getUserProfileBindings().getStampCoordinate().get() : stamp); 
		
		Stream<LatestVersion<DescriptionSememe>> descriptions = ss.getLatestDescriptionVersionsForComponent(nid);
		Optional<LatestVersion<DescriptionSememe>> desc = descriptions.filter((LatestVersion<DescriptionSememe> d) -> 
		{
			if (d.value().getDescriptionTypeConceptSequence() == MetaData.FULLY_SPECIFIED_NAME.getConceptSequence())
			{
				//shouldn't need to check for preferred, I don't believe you can have multiple FSN's.
				return true;
			}
			return false;
		}).findFirst();
		
		if (desc.isPresent())
		{
			return Optional.of(desc.get().value().getText());
		}
		else return Optional.empty();
	}
	
	/**
	 * @param nid concept nid (must be a nid)
	 * @param stamp - optional - defaults to user prefs if not provided
	 * @param language - optional - defaults to the user prefs if not provided.
	 * If provided, it should be a child of {@link IsaacMetadataAuxiliaryBinding#LANGUAGE}
	 * @return the text of the description, if found
	 */
	@SuppressWarnings("rawtypes")
	public static Optional<String> getPreferredTermForConceptNid(int nid, StampCoordinate stamp, ConceptProxy language)
	{
		SememeSnapshotService<DescriptionSememe> ss = Get.sememeService().getSnapshot(DescriptionSememe.class, 
				stamp == null ? ExtendedAppContext.getUserProfileBindings().getStampCoordinate().get() : stamp); 
		
		int langMatch = language == null ? ExtendedAppContext.getUserProfileBindings().getLanguageCoordinate().get().getLanguageConceptSequence() 
				: language.getConceptSequence();
		
		Stream<LatestVersion<DescriptionSememe>> descriptions = ss.getLatestDescriptionVersionsForComponent(nid);
		Optional<LatestVersion<DescriptionSememe>> desc = descriptions.filter((LatestVersion<DescriptionSememe> d) -> 
		{
			if (d.value().getDescriptionTypeConceptSequence() == MetaData.SYNONYM.getConceptSequence()
					&& d.value().getLanguageConceptSequence() == langMatch)
			{
				if (Frills.isDescriptionPreferred(d.value().getNid(), stamp == null ? ExtendedAppContext.getUserProfileBindings().getStampCoordinate().get() : stamp))
				{
					return true;
				}
			}
			return false;
		}).findFirst();
		
		if (desc.isPresent())
		{
			return Optional.of(desc.get().value().getText());
		}
		else return Optional.empty();
	}
	
	/**
	 * Return a sorted list of SimpleDisplayConcept objects that represent all dynamic sememe assemblages in the system (active or inactive)
	 */
	public static List<SimpleDisplayConcept> getAllDynamicSememeAssemblageConcepts()
	{
		List<SimpleDisplayConcept> allDynamicSememeDefConcepts = new ArrayList<>();

		Get.sememeService().getSememesFromAssemblage(DynamicSememeConstants.get().DYNAMIC_SEMEME_DEFINITION_DESCRIPTION.getSequence()).forEach(sememeC ->
		{
			//This will be a nid of a description - need to get the referenced component of that description
			int annotatedDescriptionNid = sememeC.getReferencedComponentNid();
			try
			{
				allDynamicSememeDefConcepts.add(new SimpleDisplayConcept(Get.sememeService().getSememe(annotatedDescriptionNid).getReferencedComponentNid()));
			}
			catch (Exception e)
			{
				LOG.error("Unexpeted error looking up dynamic sememes! with " + sememeC.toUserString(), e);
			}
		});

		Collections.sort(allDynamicSememeDefConcepts);
		return allDynamicSememeDefConcepts;
	}
	
	public static Optional<Integer> getNidForSCTID(long sctID)
	{
		SememeIndexerBI si = LookupService.get().getService(SememeIndexer.class);
		if (si != null)
		{
			//force the prefix algorithm, and add a trailing space - quickest way to do an exact-match type of search
			List<SearchResult> result = si.query(sctID + " ", true, 
					new Integer[] {MetaData.SCTID.getConceptSequence()}, 5, Long.MIN_VALUE, null);
			if (result.size() > 0)
			{
				return Optional.of(Get.sememeService().getSememe(result.get(0).getNid()).getReferencedComponentNid());
			}
		}
		else
		{
			LOG.warn("Sememe Index not available - can't lookup SCTID");
		}
		return Optional.empty();
	}
}
