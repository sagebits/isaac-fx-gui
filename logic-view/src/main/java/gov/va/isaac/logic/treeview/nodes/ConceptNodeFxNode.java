package gov.va.isaac.logic.treeview.nodes;

import java.util.function.Function;
import gov.vha.isaac.ochre.api.Get;
import gov.vha.isaac.ochre.model.logic.node.AbstractLogicNode;
import gov.vha.isaac.ochre.model.logic.node.external.ConceptNodeWithUuids;
import gov.vha.isaac.ochre.model.logic.node.internal.ConceptNodeWithSequences;

// ConceptNodeWithSequences
// ConceptNodeWithUuids
public class ConceptNodeFxNode extends AbstractTreeNodeFxNodeWithConcept {
	private final int conceptId;
	
	public ConceptNodeFxNode(ConceptNodeWithSequences logicalNode, Function<Integer, String> descriptionRenderer) {
		this(logicalNode, logicalNode.getConceptSequence(), descriptionRenderer);
	}
	public ConceptNodeFxNode(ConceptNodeWithUuids logicalNode, Function<Integer, String> descriptionRenderer) {
		this(logicalNode, Get.identifierService().getConceptSequenceForUuids(logicalNode.getConceptUuid()), descriptionRenderer);
	}
	private ConceptNodeFxNode(AbstractLogicNode logicalNode, int conceptId, Function<Integer, String> descriptionRenderer) {
		super(logicalNode, descriptionRenderer.apply(conceptId));
		this.conceptId = conceptId;
	}
	
	@Override
	public int getConceptId() {
		return conceptId;
	}
}
