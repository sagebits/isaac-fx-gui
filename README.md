ISAAC-fx-gui
=====


This code is the JavaFX GUI package for ISAAC.  To launch this GUI code, you also need an ISAAC-fx-gui-pa project to assemble the correct resources.



Note that some of this code was victim of the great ISAAC refactors - code that was broken (and not fixed) 
was removed from the package... if you are looking to recover that code, look at the git tag  "retired/pre-isaac-refactor"

Release Notes:
mvn jgitflow:release-start jgitflow:release-finish -DreleaseVersion=23.6 -DdevelopmentVersion=23.7-SNAPSHOT -DaltDeploymentRepository=maestro::default::https://va.maestrodev.com/archiva/repository/va-releases  -DdefaultOriginUrl=https://github.com/Apelon-VA/va-isaac-gui.git