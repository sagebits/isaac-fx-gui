/**
 * Copyright Notice
 * 
 * This is a work of the U.S. Government and is not subject to copyright
 * protection in the United States. Foreign copyrights may apply.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package gov.va.isaac.util;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import gov.vha.isaac.ochre.api.LookupService;
import gov.vha.isaac.ochre.api.util.WorkExecutors;

/**
 * 
 * {@link Utility}
 *
 * @author ocarlsen
 * @author <a href="mailto:daniel.armbrust.list@gmail.com">Dan Armbrust</a>
 */
public class Utility {

    public static void execute(Runnable runnable) {
        LookupService.getService(WorkExecutors.class).getExecutor().execute(runnable);
    }
    
    public static <T> Future<T> submit(Callable<T> callable) {
        return LookupService.getService(WorkExecutors.class).getExecutor().submit(callable);
    }
    
    public static Future<?> submit(Runnable runnable) {
        return LookupService.getService(WorkExecutors.class).getExecutor().submit(runnable);
    }
    
    public static <T> Future<?> submit(Runnable task, T result) {
        return LookupService.getService(WorkExecutors.class).getExecutor().submit(task, result);
    }
    
    public static ScheduledFuture<?> schedule(Runnable command, long delay, TimeUnit unit) {
        return LookupService.getService(WorkExecutors.class).getScheduledThreadPoolExecutor().schedule(command, delay, unit);
    }
    
    public static <V> ScheduledFuture<V> schedule(Callable<V> callable, long delay, TimeUnit unit) {
        return LookupService.getService(WorkExecutors.class).getScheduledThreadPoolExecutor().schedule(callable, delay, unit);
    }
    
    public static ScheduledFuture<?> scheduleWithFixedDelay(Runnable command, long initialDelay, long delay, TimeUnit unit) {
        return LookupService.getService(WorkExecutors.class).getScheduledThreadPoolExecutor().scheduleWithFixedDelay(command, initialDelay, delay, unit);
    }

    public static int compareStringsIgnoreCase(String s1, String s2) {
        int rval = 0;
        
        if (s1 != null || s2 != null) {
            if (s1 == null) {
                rval = -1;
            } else if (s2 == null) {
                rval = 1;
            } else {
                rval = s1.trim().toLowerCase().compareTo(s2.trim().toLowerCase());
            }
        }
        
        return rval;
    }
}