package gov.va.isaac.gui.conceptview.data;

import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import gov.vha.isaac.ochre.api.chronicle.LatestVersion;
import gov.vha.isaac.ochre.api.component.concept.ConceptChronology;
import gov.vha.isaac.ochre.api.component.concept.ConceptVersion;
import gov.vha.isaac.ochre.api.coordinate.StampCoordinate;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Concept extends StampedItem<ConceptVersion<?>> {
	private static final Logger LOG = LoggerFactory.getLogger(Concept.class);
	
	private boolean _hasSememes = false;
	
	@SuppressWarnings("rawtypes")
	public static ConceptVersion extractDescription(
			ConceptChronology<? extends ConceptVersion> conceptChronology,
			StampCoordinate stampCoordinate) {
		ConceptVersion concept = null;
		Optional optDS = ((ConceptChronology)conceptChronology).getLatestVersion(ConceptVersion.class, stampCoordinate);
		
		if (optDS.isPresent()) {
			LatestVersion<ConceptVersion> lv = (LatestVersion) optDS.get();
			concept = (ConceptVersion) lv.value();
		}
		
		return concept;
	}

	public static ObservableList<Concept> makeConceptVersionList(
			ConceptChronology<? extends ConceptVersion<?>> conceptChronology)
	{
		ObservableList<Concept> conceptVersionList = FXCollections.observableArrayList();
		
		for (ConceptVersion<?> conceptVersion : conceptChronology.getVersionList())
		{
			Concept concept = new Concept(conceptVersion);
			conceptVersionList.add(concept);
		}
		
		return conceptVersionList;
	}

	public Concept(ConceptVersion<?> concept)  
	{
		readStampDetails(concept);
	}
}
