/**
 * Copyright Notice
 *
 * This is a work of the U.S. Government and is not subject to copyright
 * protection in the United States. Foreign copyrights may apply.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package gov.va.isaac.gui.refexViews.refexEdit;

import java.util.function.Function;
import gov.va.isaac.gui.util.CustomClipboard;
import gov.va.isaac.gui.util.Images;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TreeTableCell;

/**
 * {@link StringCell}
 *
 * @author <a href="mailto:daniel.armbrust.list@gmail.com">Dan Armbrust</a>
 */
public class StringCell extends TreeTableCell<SememeGUI, SememeGUI>
{
	private Function<SememeGUI, String> stringFetcher_;
	
	public StringCell(Function<SememeGUI, String> stringFetcher)
	{
		stringFetcher_ = stringFetcher;
	}
	
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	protected void updateItem(SememeGUI item, boolean empty)
	{
		super.updateItem(item, empty);
		setGraphic(null);
		if (empty || item == null)
		{
			setText("");
		}
		else if (item != null)
		{
			setText(stringFetcher_.apply(item));
			ContextMenu cm = new ContextMenu();
			MenuItem mi = new MenuItem("Copy");
			mi.setGraphic(Images.COPY.createImageView());
			mi.setOnAction((action) -> 
			{
				CustomClipboard.set(stringFetcher_.apply(item));
			});
			cm.getItems().add(mi);
			setContextMenu(cm);
		}
	}
}
